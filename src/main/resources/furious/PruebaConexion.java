package furious;

import java.io.IOException;
import javax.servlet.http.*;
import java.sql.DatabaseMetaData; 
import java.sql.DriverManager; 
import java.sql.ResultSet; 
import java.sql.SQLException; 
import java.sql.*; 
@SuppressWarnings("serial")
public class PruebaConexion extends HttpServlet {
	private String url = "jdbc:mysql://localhost:3306/bebidas1"; 
    private String query = "SELECT * FROM producto"; 
    private DatabaseMetaData dbmd; 
    private Connection conexion; 
    private Statement statement; 
    private ResultSet rs;
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, world");
		
		try { 
            conexion = DriverManager.getConnection(url, "root", "");
            statement = conexion.createStatement(); 
            resp.getWriter().println(("Conexion establecida correctamente !n")
                    .toUpperCase()); 

        } catch (SQLException e) { 
        	resp.getWriter().println("No se pudo conectar !"); 
            e.printStackTrace(); 
        } 
		try { 
            rs = statement.executeQuery(query); 
            while (rs.next()) { 
                System.out.println(" " + rs.getString(1) + " -> " 
                        + rs.getString(2) + " " + rs.getString(3) + " - "
                        + rs.getString(4) + " - " + rs.getString(5) + " - "
                        + rs.getString(6)); 
            } 
            dbmd = conexion.getMetaData(); 
            resp.getWriter().println("nConexion: " + dbmd.getURL()); 
            resp.getWriter().println("Driver: " + dbmd.getDriverName()); 
            resp.getWriter().println("version: " + dbmd.getDriverVersion());
            resp.getWriter().println("DB Product Version: " 
                    + dbmd.getDatabaseProductVersion()); 

        } catch (SQLException e) { 
        	resp.getWriter().println(("error al realizar la consulta con la base de datos.")
                            .toUpperCase()); 
            e.printStackTrace(); 
        } 
	}
}
