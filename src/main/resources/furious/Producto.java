package furious;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Java.Mysql;

public class Producto extends HttpServlet {


	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, world Producto");
		String query = "SELECT * FROM producto"; 
		DatabaseMetaData dbmd=null; 
		Connection conexion = Mysql.getConection();; 
		Statement statement = null;
		ResultSet rs;
		try {
			statement = conexion.createStatement();
		} catch (SQLException e1) {
			System.out.println(e1.getMessage());
		} ; 
		try { 
			rs = statement.executeQuery(query); 
			
			while (rs.next()) { 
				String cod = rs.getString(1);
				String nom = rs.getString(2);
				String prov = rs.getString(3);
				String tip = rs.getString(4);
				String preUni = rs.getString(5);
				String sto = rs.getString(6);

			    resp.getWriter().print( "<div class='row' id='$cod' onclick='cambio("+cod+");'>");
			    resp.getWriter().print( "<div class='cell' id='"+"$cod"+"cod"+"'>"+cod+"</div>");
			    resp.getWriter().print( "<div class='cell' id='"+"$cod"+"nom"+"'>"+nom+"</div>");
			    resp.getWriter().print( "<div class='cell' id='"+"$cod"+"prov"+"'>"+prov+"</div>");
			    resp.getWriter().print( "<div class='cell' id='"+"$cod"+"tip"+"'>"+tip+"</div>");
			    resp.getWriter().print( "<div class='cell' id='"+"$cod"+"preunip"+"'>"+preUni+"</div>");
			    resp.getWriter().print( "<div class='cell' id='"+"$cod"+"sto"+"'>"+sto+"</div>");
			    resp.getWriter().print( "</div>");
				System.out.println(" " + rs.getString(1) + " -> " 
						+ rs.getString(2) + " " + rs.getString(3)); 
			} 
			dbmd = conexion.getMetaData(); 
		} catch (SQLException e) { 
			resp.getWriter().println(("error al realizar la consulta con la base de datos.")
					.toUpperCase()); 
			System.out.println(e.getMessage());; 
		} 
		//System.out.println(" no paso MEE");
	}
}
